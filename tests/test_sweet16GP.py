from unittest import TestCase


class TestSweet16GP(TestCase):

    def test_bit_set(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        self.assertEqual(0b10000000, cpu.bit_set(0b00000000, 7), 'Failed to set MSb bit')
        self.assertEqual(0b01000000, cpu.bit_set(0b00000000, 6), 'Failed to set bit')

    def test_bit_clear(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        self.assertEqual(0b01111111, cpu.bit_clear(0b11111111, 7), 'Failed to clear MSb bit')
        self.assertEqual(0b10111111, cpu.bit_clear(0b11111111, 6), 'Failed to clear bit')

    def test_bit_toggle(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        self.assertEqual(0b10000000, cpu.bit_set(0b00000000, 7), 'Failed to detect MSb bit')
        self.assertEqual(0b01000000, cpu.bit_set(0b00000000, 6), 'Failed to detect MSb bit')

    def test_bit_test(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        self.assertEqual(1, cpu.bit_test(0xA0, 7), 'Failed to detect MSb bit')
        self.assertEqual(0, cpu.bit_test(~0xA0, 7), 'Failed to detect MSb bit')

    def test_cold_boot(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.cold_boot(16)
        self.assertEqual(16, len(cpu.MEM), 'Failed to initialize memory')
        self.assertEqual(8, len(cpu.REGFILE), 'Failed to initialize the register file')

    def test_warm_boot(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()

        cpu.REGFILE[cpu.ACC] = 0xffff
        cpu.REGFILE[cpu.RETSTACK] = 0xffff
        cpu.REGFILE[cpu.COMP] = 0xffff
        cpu.REGFILE[cpu.STATUS] = 0xffff
        cpu.REGFILE[cpu.PC] = 0xffff

        cpu.warm_boot()
        self.assertEqual(0, cpu.REGFILE[cpu.ACC], 'Failed to clear ACC register (R0)')
        self.assertEqual(0, cpu.REGFILE[cpu.RETSTACK], 'Failed to clear RETSTACK register (R4)')
        self.assertEqual(0, cpu.REGFILE[cpu.COMP], 'Failed to clear COMP register (R5)')
        self.assertEqual(0, cpu.REGFILE[cpu.STATUS], 'Failed to clear STATUS register (R6)')
        self.assertEqual(0, cpu.REGFILE[cpu.PC], 'Failed to clear PC register (R7)')

    def test_init_regfile(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        self.assertEqual(8, len(cpu.REGFILE), 'Failed to initialize the register file')

    def test_get_register(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.REGFILE[cpu.ACC] = 0xfafc
        self.assertEqual(0xfafc, cpu.get_register(cpu.ACC), 'Failed to get register ACC (R0)')

    def test_get_register_lsb(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.REGFILE[cpu.ACC] = 0xfafc
        self.assertEqual(0xfc, cpu.get_register_lsb(cpu.ACC), 'Failed to get register ACC (R0) lsb')

    def test_get_register_msb(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.REGFILE[cpu.ACC] = 0xfafc
        self.assertEqual(0xfa, cpu.get_register_msb(cpu.ACC), 'Failed to get register ACC (R0) msb')

    def test_set_register(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.set_register(cpu.ACC, 0xfccd)
        self.assertEqual(0xfccd, cpu.get_register(cpu.ACC), 'Failed to get register ACC (R0)')

    def test_set_register_lsb(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.set_register(cpu.ACC, 0xf0ac)
        cpu.set_register_lsb(cpu.ACC, 0xff)
        self.assertEqual(0xf0ff, cpu.get_register(cpu.ACC), 'Failed to get register ACC (R0)')

    def test_set_register_msb(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.set_register(cpu.ACC, 0xafcd)
        cpu.set_register_msb(cpu.ACC, 0xff)
        self.assertEqual(0xffcd, cpu.get_register(cpu.ACC), 'Failed to get register ACC (R0)')

    def test_init_ram(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.init_ram(32)
        self.assertEqual(32, len(cpu.MEM), 'Failed to initialize memory')

    def test_poke_byte(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.poke_byte(0x0002, 0xA0)
        self.assertEqual(0xA0, cpu.peek_byte(0x0002), 'Failed to poke memory byte')

    def test_peek_byte(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.MEM[0x10] = 0xFE
        self.assertEqual(0xFE, cpu.peek_byte(0x0010), 'Failed to poke memory byte')

    def test_poke_word(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.poke_word(0x0008, 0xacfb)
        self.assertEqual(0xfb, cpu.MEM[0x0008], 'Failed to poke memory word  low byte')
        self.assertEqual(0xac, cpu.MEM[0x0009], 'Failed to poke memory word high byte')

    def test_peek_word(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.poke_word(0x0010, 0xacfb)
        self.assertEqual(0xacfb, cpu.peek_word(0x0010), 'Failed to peek memory word')

    def test_set_flags(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.set_flags('C')
        self.assertEqual(1, cpu.test_flag('C'), 'Failed to set Carry Flag')
        cpu.set_flags('Z')
        self.assertEqual(1, cpu.test_flag('Z'), 'Failed to set Zero Flag')
        cpu.set_flags('V')
        self.assertEqual(1, cpu.test_flag('V'), 'Failed to set Overflow Flag')
        cpu.set_flags('N')
        self.assertEqual(1, cpu.test_flag('N'), 'Failed to set Negative Flag')

    def test_clear_flags(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.REGFILE[cpu.STATUS] = 0x0b11000011
        cpu.clear_flags('C')
        self.assertEqual(0, cpu.test_flag('C'), 'Failed to clear Carry flag')
        cpu.clear_flags('N')
        self.assertEqual(0, cpu.test_flag('N'), 'Failed to clear Negative flag')
        cpu.clear_flags('Z')
        self.assertEqual(0, cpu.test_flag('Z'), 'Failed to clear Zero flag')
        cpu.clear_flags('V')
        self.assertEqual(0, cpu.test_flag('V'), 'Failed to clear Overflow flag')

    def test_test_flag(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.set_flags('CZVN')
        self.assertEqual(1, cpu.test_flag('C'), 'Failed to clear Carry flag')
        self.assertEqual(1, cpu.test_flag('Z'), 'Failed to clear Zero flag')
        self.assertEqual(1, cpu.test_flag('V'), 'Failed to clear Overflow flag')
        self.assertEqual(1, cpu.test_flag('N'), 'Failed to clear Negative flag')

    def test_set_value_flags(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        cpu.set_value_flags(0b1000_0000_0000_0000)
        self.assertEqual(1, cpu.test_flag('N'), 'Failed to set value flag N')
        cpu.set_value_flags(0b0000_0000_0000_0000)
        self.assertEqual(1, cpu.test_flag('Z'), 'Failed to set value flag Z')

    def test_set_overflow_flags(self):
        from sweet16gp import Sweet16GP
        cpu = Sweet16GP()
        v1 = 0b1111_1111_1111_1111
        v2 = 0b1000_1111_0000_1010
        sum = (v1 + v2) & 0xffff
        cpu.set_overflow_flags(v1, v2, sum)
        self.assertEqual(0, cpu.test_flag('V'), 'Failed to clear overflow flag')
        v1 = 0b0111_1111_1111_1111
        v2 = 0b0000_1111_0000_1010
        sum = (v1 + v2) & 0xffff
        sum = (sum | 0b1000_0000_0000_0000) & 0xffff
        cpu.set_overflow_flags(v1, v2, sum)
        self.assertEqual(1, cpu.test_flag('V'), 'Failed to set overflow flag')
