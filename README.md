# Sweet16-GP CPU & Toolings

### Purpose
The purpose of this project is to provide a complete 
implementation life cycle for a small CPU design and
it's tooling (emulator, assembler, compiler, hardware, 
etc.) The CPU design is a simple 8/16 bit Accumulator 
machine designed and implemented on a small FPGA. 
This project provides a basis for introducing CS/EE 
concepts to workshop attendees and students. Students 
should have some basic programming skills and a basic 
understanding of digital logic. However, nothing beyond
a hobbiest or entry level knowledge is required due to 
the simplicity of the CPU and tooling design. The 
emulator and CPU design are very simple yet realistic 
and usable in small hardware projects. The Assembler 
is a simple 2-pass design and the compiler is where most 
of the real complexity lies. Even though, the compiler 
has a clean understandable deisgn and is intended as a 
practical introduction instead of a more complex 
theoretical introduction to compiler design. 

### Overview
The Sweet16-GP CPU is a small 8/16 bit processor 
design modeled after Steve Wozniak's Sweet16 emulated 
processor designed to ease the implementation of 
Apple BASIC on the 6502.

This modified implementation provides a genral purpose
processor design that is realistic, yet maintains
the simplicity of the original design. Making a perfect 
instructional tool for CS and EE students alike.

The articles posted here: 
    [www.randallmorgan.me](http://www.randallmorgan.me/2019/08/06/sweet16-an-8-16-design/)
    
Contain information and code. The articles are being 
written as of August 2019. If code for a part of the 
project is not included here it is because the articles 
covering that portion of the project have not yet been 
completed. However, almost all of the software has been 
written.

This project includes:
   * A Software Emulator
   * An Assembler
   * A High Level Language Compiler(s)
   * A logisim Implementation
   * A VHDL Implementation suitable for loading on to an FPGA

